﻿using Microsoft.AspNet.Identity.EntityFramework;
using MVC.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult APICall()
        {
            ViewBag.Message = "Your API CALL Page.";
            ViewBag.ShowList = Session["Name"].ToString() == "Admin" ? true : false; ;

            ViewBag.Content = TempData["Content"];
            return View();
        }

       
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                string authenticated = AuthorizeUserAccess.IsAuthorized(model.Name, model.Password);
                if (authenticated != "NotFound")
                {
                    Session["Name"] = authenticated;
                    TempData["Message"] = authenticated == "Admin" ? true : false;
                    return Redirect(Url.Action("APICall", "Home"));
                }


            }
            return Redirect(Url.Action("Index", "Home"));
        }

        [AuthorizeUserAccess]
        public ActionResult ViewApp()
        {
            var client = new RestClient("https://onesignal.com/api/v1/apps/ef2ac3e3-30af-4e54-862e-588cff9b7fce");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Basic NWZhODQ0MzctYzNhYi00YmNhLWE4ZDItMmY2MzUzZTEzMTlh");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "__cfduid=d9963645becb49b2e8c0fb6240160e6231600731864");
            IRestResponse response = client.Execute(request);
            TempData["Content"] = response.Content;

            return Redirect(Url.Action("APICall", "Home"));
        }

        [AuthorizeUserAccess]
        public ActionResult CreateApp()
        {
            var client = new RestClient("https://onesignal.com/api/v1/apps?name=newApp");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Basic NWZhODQ0MzctYzNhYi00YmNhLWE4ZDItMmY2MzUzZTEzMTlh");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "__cfduid=d9963645becb49b2e8c0fb6240160e6231600731864; _mkra_stck=6be575ad105b3a65a1668ee964c369c2%3A1600736397.295242");
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            TempData["Content"] = response.Content;

            return Redirect(Url.Action("APICall", "Home"));
        }

        [AuthorizeUserAccess]
        public ActionResult UpdateApp()
        {

            var client = new RestClient("https://onesignal.com/api/v1/apps/ef2ac3e3-30af-4e54-862e-588cff9b7fce?name=CrudApp");
            client.Timeout = -1;
            var request = new RestRequest(Method.PUT);
            request.AddHeader("Authorization", "Basic NWZhODQ0MzctYzNhYi00YmNhLWE4ZDItMmY2MzUzZTEzMTlh");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "__cfduid=d9963645becb49b2e8c0fb6240160e6231600731864; _mkra_stck=6be575ad105b3a65a1668ee964c369c2%3A1600774554.0216289");
            IRestResponse response = client.Execute(request);
            TempData["Content"] = response.Content;

            return Redirect(Url.Action("APICall", "Home"));
        }

    }
}