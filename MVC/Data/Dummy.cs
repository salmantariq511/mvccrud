﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Data
{
    public class Dummy
    {
        public static List<Employee> GetEmployees(MVCDB context)
        {
            List<Employee> employees = new List<Employee>()
            {
            new Employee()
            {
                EmployeeName = "John",
                EmployeeType = "Admin",
                EmployeePassword = "admin"
            },
            new Employee()
            {
                 EmployeeName = "Daniel",
                EmployeeType = "DataEntry",
                EmployeePassword = "admin"
            }
            };

            return employees;
        }
    }
}