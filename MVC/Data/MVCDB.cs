﻿using Microsoft.AspNet.Identity.EntityFramework;
using MVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVC.Data
{
    public class MVCDB: DbContext
    {
        public MVCDB() : base("DefaultConnection")
        {

        }

        public DbSet<Employee> Employees { get; set; }
    }
}