﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MVC.Models
{
    //public class Employee : IdentityUser
    public class Employee 
    {

        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Employee> manager)
        //{
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        //    // Add custom user claims here
        //    return userIdentity;
        //}
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeePassword { get; set; }
    }

    //public class EmployeeDbContext : IdentityDbContext<Employee>
    //{
    //    public EmployeeDbContext()
    //        : base("DefaultConnection", throwIfV1Schema: false)
    //    {
    //    }

    //    public static EmployeeDbContext Create()
    //    {
    //        return new EmployeeDbContext();
    //    }
    //}
}