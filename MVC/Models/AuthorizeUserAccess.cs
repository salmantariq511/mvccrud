﻿using Microsoft.Owin.Security.Provider;
using MVC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;

namespace MVC.Models
{
    public class AuthorizeUserAccess:AuthorizeAttribute
    {
        
        public string EmployeeType { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var rd = httpContext.Request.RequestContext.RouteData;
            string currentAction = rd.GetRequiredString("action");
            EmployeeType = httpContext.Session["Name"].ToString();
            
            if (EmployeeType.Contains("Admin"))
            {
                return true;
            } if (EmployeeType.Contains("DataEntry") && currentAction == "ViewApp")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string IsAuthorized(string name, string password)
        {
            MVCDB db = new MVCDB();
            var user = db.Employees.Where(x => x.EmployeeName.ToLower() == name.ToLower() && x.EmployeePassword == password).FirstOrDefault();
            
            if(user != null)
            {
                return user.EmployeeType;
            }
            else
            {
                return "NotFound";
            }
        }
    }
}