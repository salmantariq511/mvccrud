﻿


First Scenario

1. Go to Dummy.cs file in "Data" folder to view the employee credentials for sign in purpose.

2. Two employees are added by default, their credentials are in Dummy.cs file, available in Data Folder for sign in purpose.

3. Use any credentials to sign in and you will view the options according to user role.

4. To create an app go to HomeController Function "CreateAPP" and change the name of the app in first line of code in query parameter according to your desired name.

5. To update an app go to HomeController Function "UpdateAPP" and change the name of the app in first line of code in query parameter with any of your existing apps names.

6. Response result is displayed as an alert.


NOTE: IF THE ABOVE SCENARIO DOES NOT WORK THEN PLEASE FOLLOW THE STEPS MENTIONED IN SECOND SCENARIO BELOW FOR SETTING UP YOUR DATABASE.
---------------------------------------------------------------------------------------------------------------------------------------------------------------


Second Scenario

1. Select the "APP_Data" folder and click show all files option through solution explorer.

2. Delete the existing databases.

3. Delete the folder Migrations if exists.

4. Open Migrations.txt file in Docs folder and execute all three commands in a sequence mentioned in the file.

5. Two employees are added by default, their credentials are in Dummy.cs file, available in Data Folder for sign in purpose.

6. Use any credentials to sign in and you will view the options according to user role.

7. To create an app go to HomeController Function "CreateAPP" and change the name of the app in first line of code in query parameter according to your desired name.

8. To update an app go to HomeController Function "UpdateAPP" and change the name of the app in first line of code in query parameter with any of your existing apps names.

9. Response result is displayed as an alert.

