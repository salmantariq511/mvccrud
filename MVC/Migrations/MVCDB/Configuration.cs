﻿namespace MVC.Migrations.MVCDB
{
    using MVC.Data;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVC.Data.MVCDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\MVCDB";
        }

        protected override void Seed(MVC.Data.MVCDB context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Employees.AddOrUpdate(
              e => new { e.EmployeeName, e.EmployeeType, e.EmployeePassword }, Dummy.GetEmployees(context).ToArray());
        }
    }
}
